# Calculo Numerico - 2018 de la Universidad del Comahue. Centro Regional Bariloche

## Profesorado y Licenciatura en Matematicas
http://crubweb.uncoma.edu.ar/

### Dr. René Cejas Bolecek
### rene.bolecek@gmail.com

Material de teoría, prácticas y algunos de los módulos en python utilizados en la materia. 

## Calendario 2018 2do cuatrimestre:

16/08: U1. Sistema posicional. Conversión entre sistemas numéricos. La arquitectura Von Neumann de una computadora. Representación de números enteros: punto fijo (representación signo-magnitud y complemento a dos).

23/08: U1. Aritmética con enteros complemento a dos. Punto flotante: simple y doble presición. Propiedades de la aritmética de punto flotante. Estándar IEEE. Introducción a los errores por 
redondeo y truncamiento. Error absoluto y relativo.

30/08: U1. Aritmética a dígitos finitos. Ejemplos. Multiplicaciones anidadas (cálculo de raíces de polinomios). 

06/09: U1. Estimación de errores numéricos en la derivada y en la integración. Caracterización de algoritmos iterativos. Rapidez de convergencia. U2. Repaso álgebra lineal (matrices, espacios vectoriales y Teorema de Rouche-Frobenius).

13/09: U2 Resolución del SEL triangular inferior y superior. Implementación en python.  Método Gauss. Número de condicionamiento. Métodos factorización LU. Teoremas.

17 al 22 de Septiembre: Semana de exámenes (según calendario académico)

20/09: U2. Método Doolittle. Método Cholesky. Pivoteo. Matrices definidas positivas. Matrices especiales. Inversa. Métodos iterativos estacionarios. Método de Gauss-Seidel. Método Jacobi. U3. Interpolación y aproximación polinómica. Teorema de Weierstrass. Polinomios de Lagrange. Método de Newton. Teoremas solucion única y errores. Método de Neville. Limitaciones de las interpolaciones polinómicas.

24/09: Primer parcial ( Incluye todos los temas anteriores)

Temas siguientes clases a confirmar...

licence: MIT. http://opensource.org/licenses/MIT 
# Calculo Numerico - 2018 de la Universidad del Comahue. Centro Regional Bariloche

## Profesorado y Licenciatura en Matematicas
http://crubweb.uncoma.edu.ar/

### Dr. René Cejas Bolecek
### rene.bolecek@gmail.com


Notebooks en python utilizados en la teoría de la materia.
Ejecutar el código con

ipython notebook nombreDelNotebook.ipynb

o

jupyter notebook nombreDelNotebook.ipynb

licence: MIT. http://opensource.org/licenses/MIT 